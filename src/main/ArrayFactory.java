package main;

import java.util.ArrayList;

public class ArrayFactory {

    private int x;

    public ArrayFactory(int x) {
        this.x = x;
    }

    public int[] oneDimension() {
        int[] tab = new int[this.x];
        System.out.println(tab);
        return tab;
    }

    public int[][] twoDimension() {
        int[][] tab = new int[this.x][this.x];
        System.out.println(tab);
        return tab;
    }

    public void yDimension(int y){

        int[][] tab2 = new int[2][2];
        int[][] tab3 = new int[2][2];
        tab2[0][0] = 1;
        tab2[0][1] = 1;
        tab2[1][0] = 1;
        tab2[1][1] = 1;
        System.out.println(tab2);



    }



}