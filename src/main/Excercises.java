package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class Excercises {


    public void makeIntegerList(int limitValue){
        List<Integer> list = new ArrayList<>();

        for( int i = 1; i <= limitValue; i ++){
           list.add(i);
        }
        System.out.println(list);
    }



    public List<Integer> makeIntegerList2(int limitValue){
        List<Integer> list = new ArrayList<>();
        int i = 1;
        while (i <= limitValue){
            list.add(i);
            i++;
        }
        System.out.println(list);
        return list;
    }

    public void printFibonacciSequenceElements(int numberOfElements){
        List<Integer> sequence = new ArrayList<>();
        int i = 0;

        while (i <= numberOfElements) {
            if (i == 0){
                sequence.add(0);
                i ++;
            }
            if (i == 1){
                sequence.add(1);
                i ++;
            }
            else {
                int k = sequence.get(sequence.size() - 1);
                int l = sequence.get(sequence.size() - 2);
                int m = k + l;
                sequence.add(m);
                i ++;
            }
        }
        System.out.println(sequence);

    }

    public void firstNumbersListMaker(int x){
        int i = 1;
        List<Integer> firstNumberList = new ArrayList<>();

        while (i <= x){
            if(firstNumberVerifier(i)){
                firstNumberList.add(i);
            }
            i++;
        }
        if (firstNumberList.isEmpty()){
            System.out.println("nie ma liczb pierwszych mniejszych lub równych " + x);
        }
        System.out.println("Liczby pierwsze mniejsze lub równe " + x + " to: " + firstNumberList);
    }

    private boolean firstNumberVerifier(int number){
        int j = 1;
        List<Integer> dividersList = new ArrayList<>();

        while (j <= number) {
            if (number % j == 0) {
                dividersList.add(j);
            }
            j++;
        }
        if(dividersList.size() == 2){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean canConstructRectangularTriangle(int number1, int number2, int number3){
        List<Integer> list = new ArrayList<>();
        list.add(number1);
        list.add(number2);
        list.add(number3);
        Collections.sort(list);
        int a = list.get(0);
        int b = list.get(1);
        int c = list.get(2);
        if(((a*a)+(b*b)) == (c*c)){
            System.out.println(" z podanych liczb stworzymy trójkąt prostopadły");
            return true;
        }
        else{
            System.out.println(" z podanych liczb nie stworzymy trójkąta prostopadłego");
            return false;
        }
    }


    public double returnSquareRoot(int number){
        double result = Math.sqrt(number);
        System.out.println(result);
        return result;
    }

    public boolean isEven(int number){
        if (number % 2 == 0) {
            System.out.println("true");
            return true;
        }

        else {
            System.out.println("false");
            return false;
        }
    }

    public int toThePowerOf3(int powerableNumber){
       int result = powerableNumber * powerableNumber * powerableNumber;
        System.out.println(result);
        return result;
    }

    public boolean isDivisibleByTwoNumbers(int number1, int number2, int divisibleNumber){

        if((divisibleNumber % number1 == 0) && (divisibleNumber % number2 ==0)){
            System.out.println("true");
            return true;
        }
        else{
            System.out.println("false");
            return false;
        }

    }


    public void crazyCalculator(int firstNb, int secondNb) {

        int a = firstNb - secondNb;
        int b = firstNb + secondNb;
        int c = firstNb * secondNb;
        System.out.println(a + " " + b + " " + c);
    }


}
