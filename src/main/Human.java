package main;

public class Human {

    private int age;
    private int weight;
    private int height;
    private String name;
    private boolean male;

    public Human(int age, int weight, int height, String name, boolean male) {
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.name = name;
        this.male = male;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public String getName() {
        return name;
    }

    public boolean isMale() {
        return male;
    }
}
