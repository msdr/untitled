package main;

public class MyNumber {
    private int nr;

    public MyNumber(int constructionNumber) {
        this.nr = constructionNumber;
    }

    public boolean isOdd(){
        if (this.nr % 2 == 1){
            System.out.println("jest nieparzysta");
            return true;
        }
        else return false;
    }
    public boolean isEven(){
        if (this.nr % 2 == 0){
            System.out.println("jest parzysta");
            return true;
        }
        else return false;
    }

    public double sqrt(){
        double result = Math.sqrt(this.nr);
        System.out.println("pierwiastek to: " +result);
        return result;
    }

    public int pow(MyNumber x){
        int result = (int) Math.pow(this.nr, x.nr);
        System.out.println(this.nr + " do potęgi " + x.nr + " wynosi: " + result);
        return result;
    }

    public MyNumber add(MyNumber x){
        return new MyNumber(this.nr + x.nr);
    }

    public MyNumber subtract(MyNumber x){
        return new MyNumber(this.nr - x.nr);
    }


    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }
}
