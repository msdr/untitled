package main;

public class Rectangle {

    private double bokA;
    private double bokB;

    public Rectangle(double bokA, double bokB) {
        this.bokA = bokA;
        this.bokB = bokB;
    }

    public double obliczPole(Rectangle rectangle){
        double polePowierzchni = rectangle.getBokA() * rectangle.getBokB();
        System.out.println("pole powierzchni: " + polePowierzchni);
        return polePowierzchni;
    }

    public double obliczObwod(Rectangle rectangle){
        double obwod = 2 * (rectangle.getBokB() + rectangle.getBokB());
        System.out.println("obwód: " + obwod);
        return obwod;
    }

    public double obliczDlugoscPrzekatnej(Rectangle rectangle){
        double dlugoscPrzekatnej = Math.sqrt((rectangle.getBokA() * rectangle.getBokA()) + (rectangle.getBokB() * rectangle.getBokB()));
        System.out.println("długosc przekatnej: " + dlugoscPrzekatnej);
        return  dlugoscPrzekatnej;
    }
    public double getBokA() {
        return bokA;
    }

    public double getBokB() {
        return bokB;
    }
}
