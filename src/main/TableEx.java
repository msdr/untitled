package main;

import java.util.ArrayList;
import java.util.List;

public class TableEx {


    public String[] ourNewTab() {
        String[] tab = {"a", "b", "c", "d", "e"};
        System.out.println(tab);
        return tab;
    }


    public int[] switcherooTab(int[] tab) {
        int a = tab[0];
        int b = tab[1];
        int c = tab[2];
        int[] tab2;
        tab2 = new int[]{c, b, a};
        System.out.println(tab2[0]);
        System.out.println(tab2[1]);
        System.out.println(tab2[2]);
        return tab2;
    }




    public void numberToBinaryNumber(int numberToProcess) {
        List<Integer> list = new ArrayList<>();
        List<Integer> reversedList = new ArrayList<>();
        while (numberToProcess > 0) {
            if (numberToProcess % 2 == 0) {
                list.add(0);
                numberToProcess = numberToProcess / 2;
            }
            if (numberToProcess % 2 == 1) {
                list.add(1);
                numberToProcess = numberToProcess / 2;
            }
        }
        int i = list.size();
        String stringResult = "";
        while (i > 0) {
            reversedList.add(list.get(i - 1));
            stringResult += list.get(i - 1);
            i--;
        }

        String result = reversedList.toString();
        System.out.println(result);
        System.out.println(stringResult);
        String test = Integer.toBinaryString(numberToProcess);
        System.out.println(test);

    }
/*
    public void printBinary(int number) {
        String binaryReversed = "";
        while (number > 0) {
            // This is isn't optimal ;)
            // Read about StringBuilder/StringBuffer
            binaryReversed += number % 2;
            number /= 2;
        }
        System.out.println(reverse(binaryReversed));
    }

    private  String reverse(String word) {
        char[] chars = word.toCharArray();
        for (int currentCharIndex = 0; currentCharIndex < chars.length / 2; currentCharIndex++) {
            char tempChar = chars[currentCharIndex];
            int curretnCharFromEndIndex = chars.length - currentCharIndex - 1;

            chars[currentCharIndex] = chars[curretnCharFromEndIndex];
            chars[curretnCharFromEndIndex] = tempChar;
        }
        String result = "";
        for (int i = 0; i < chars.length; i++) {
            result += chars[i];
        }
        return result;
    }
*/
    public void breakNumberToPieces(int numberToPrecess) {
        String numericValue = String.valueOf(numberToPrecess);
        int length = numericValue.length();
        char[] charSequence = numericValue.toCharArray();
        int i = 0;
        while (length > 0) {
            System.out.println(charSequence[length - 1]);
            length = length - 1;
        }

    }


    public void isStringPalindrom(String wordToCheck) {
        String reversedWord = mirrorCharSequence(wordToCheck);
        if (reversedWord.equals(wordToCheck)) {
            System.out.println("słowo jest palindromem");
        } else {
            System.out.println("słowo nie jest palindromem");
        }


    }


    public String mirrorCharSequence(String stringToReverse) {
        char[] charToReverse = stringToReverse.toCharArray();
        int wordLength = charToReverse.length;
        char[] reversedCharSequence = new char[wordLength];
        int i = wordLength - 1;
        int rcsIndex = 0;
        while (i >= 0) {
            char j = charToReverse[i];
            reversedCharSequence[rcsIndex] = j;
            rcsIndex++;
            i--;
        }
        String result = String.valueOf(reversedCharSequence);
        return result;
    }


}
